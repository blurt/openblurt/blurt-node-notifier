 ## Blurt Node Notifier

Blurt Node Notifier is a monitoring software for Blurt nodes written in Go that sends notifications to a Discord channel via a webhook when a node goes down or out of sync.


## Prerequisites

You need to have Go installed on your system.
Visit https://go.dev/ if you need to install Go.


## Installation

First, clone the git repo, compile the binary, and move it to `usr/local/bin` or wherever is appropriate for your system.

```bash
git clone https://gitlab.com/blurt/openblurt/blurt-node-notifier.git
cd blurt-node-notifier
go build -o blurt-node-notifier .
mv blurt-node-notifier /usr/local/bin
```

Create your config file. Use `example-config.json` as a reference.
You can put your config file wherever you like.

## Usage

Set the `NOTIFIER_CONFIG` environment variable and run blurt-node-notifier:

```bash
NOTIFIER_CONFIG=/path/to/config.json blurt-node-notifier
```

## Contributing

Merge requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)