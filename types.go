package main

import (
	"encoding/json"
	"fmt"
	"os"
	"sync"
)

type RpcResponse struct {
	JSONRPC string          `json:"jsonrpc"`
	Result  json.RawMessage `json:"result,omitempty"`
	Error   *RpcError       `json:"error,omitempty"`
	ID      int             `json:"id"`
}

type RpcError struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
}

type DynamicGlobalProps struct {
	Time string `json:"time"`
}

type State struct {
	mu         sync.RWMutex
	NodeStates map[string]*NodeState
}

type NodeState struct {
	LastState        string
	LastErrorMessage string
}

func (s *State) GetLastState(name string) *NodeState {
	s.mu.RLock()
	defer s.mu.RUnlock()
	return s.NodeStates[name]
}

func (s *State) UpdateState(name, lastState string, lastErrorMessage error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	var state *NodeState = s.NodeStates[name]
	state.LastState = lastState
	if lastErrorMessage == nil {
		state.LastErrorMessage = ""
	} else {
		state.LastErrorMessage = lastErrorMessage.Error()
	}
}

type Config struct {
	DiscordWebhook string            `json:"discord_webhook"`
	DicordRoleId   string            `json:"discord_role_id"`
	Nodes          map[string]string `json:"nodes"`
	Interval       int               `json:"interval"`
}

func LoadConfig() Config {
	filename := os.Getenv("NOTIFIER_CONFIG")
	fmt.Println("Filename:", filename)
	if filename == "" {
		panic("Config file is missing.")
	}
	f, err := os.Open(filename)
	if err != nil {
		fmt.Println("config.json is missing.")
		panic(err)
	}

	var c Config
	dec := json.NewDecoder(f)
	err = dec.Decode(&c)
	if err != nil {
		panic(err)
	}
	fmt.Println("Successfully loaded config.")
	return c
}

type OutOfSyncError struct {
	BlockAge int
}

func (e *OutOfSyncError) Error() string {
	return fmt.Sprintf("Block Age: %v", e.BlockAge)
}
