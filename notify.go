package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

func notify(url, msg string) error {
	payload := map[string]string{"content": msg}
	json_payload, jsErr := json.Marshal(payload)

	if jsErr != nil {
		return fmt.Errorf("notify json marshall error: %w", jsErr)
	}

	res, resErr := http.Post(url, "application/json", bytes.NewBuffer(json_payload))
	if resErr != nil {
		return fmt.Errorf("notify response error: %w", resErr)
	}
	defer res.Body.Close()

	if !(res.StatusCode >= 200 && res.StatusCode <= 204) {
		return fmt.Errorf("notify error: returned status code %v", res.StatusCode)
	}
	return nil
}
