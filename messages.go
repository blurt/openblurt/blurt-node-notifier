package main

import (
	"fmt"
)

func PrettifyMsg(role string, node string, msgType string, state string, msg string) string {
	message := ""
	switch msgType {
	case "error":
		message = fmt.Sprintf(`
%s,
	node: %s
	status: :broken_heart:
	state:  %s
	msg: %s`, role, node, state, msg)

	case "warning":
		message = fmt.Sprintf(`
%s,
	node: %s
	status: :warning:
	state: %s
	msg: %s`, role, node, state, msg)

	case "success":
		message = fmt.Sprintf(`
%s,
	node: %s
	status: :white_check_mark:
	state:  %s
	msg: %s`, role, node, state, msg)
	}

	return message
}
