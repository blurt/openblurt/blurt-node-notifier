package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"
)

func healthCheck(name, url string) (int, string, error) {
	jsonBody := []byte(`{"jsonrpc":"2.0","id":0,"method":"database_api.get_dynamic_global_properties"}`)
	bodyReader := bytes.NewReader(jsonBody)

	var requestURL string = url
	req, reqErr := http.NewRequest(http.MethodPost, requestURL, bodyReader)
	if reqErr != nil {
		return 500, "", fmt.Errorf("healthCheck request error on %v: %w", name, reqErr)
	}

	res, resErr := http.DefaultClient.Do(req)
	if resErr != nil {
		return 500, "", fmt.Errorf("healthCheck response error on %s: %w", name, resErr)
	}

	defer res.Body.Close()

	if res.StatusCode >= 400 {
		return res.StatusCode, "", fmt.Errorf("health check on %s returned status code %v", name, res.StatusCode)
	}

	resBody, bodyReadErr := io.ReadAll(res.Body)
	if bodyReadErr != nil {
		return 500, "", fmt.Errorf("healthCheck body read error on %s: %w", name, bodyReadErr)
	}

	rpcRes := new(RpcResponse)
	resJsonErr := json.Unmarshal(resBody, &rpcRes)
	if resJsonErr != nil {
		return 500, "", fmt.Errorf("healthCheck response unmarshall error on %s: %w", name, resJsonErr)
	}

	if rpcRes.Error != nil {
		return 500, "", fmt.Errorf("%s returned an error response: %v", name, string(resBody))
	}

	dgp := new(DynamicGlobalProps)
	dgpJsonErr := json.Unmarshal(rpcRes.Result, &dgp)
	if dgpJsonErr != nil {
		return 500, "", fmt.Errorf("healthCheck dgp unmarshall error on %s: %w", name, reqErr)
	}
	currentTime := time.Now().UTC()
	serverTime, serverTimeParseErr := time.Parse("2006-01-02T15:04:05", dgp.Time)
	if serverTimeParseErr != nil {
		return 500, "", fmt.Errorf("healthCheck server time parse error on %s: %w", name, serverTimeParseErr)
	}
	blockAge := currentTime.Sub(serverTime)

	if blockAge <= 60*time.Second {
		return res.StatusCode, fmt.Sprintf("Block age: %v", blockAge), nil
	} else {
		return res.StatusCode, fmt.Sprintf("%s is out of sync; block age: %v\n", name, blockAge), &OutOfSyncError{BlockAge: int(blockAge)}
	}
}
