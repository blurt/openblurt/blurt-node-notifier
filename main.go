package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	var cfg Config = LoadConfig()

	discordWebhook := cfg.DiscordWebhook
	discordRole := cfg.DicordRoleId
	nodes := cfg.Nodes
	interval := time.Duration(cfg.Interval) * time.Second

	var ErrorStates *State = &State{
		NodeStates: make(map[string]*NodeState),
	}

	for nodeName := range nodes {

		ErrorStates.NodeStates[nodeName] = &NodeState{
			LastState:        "healthy",
			LastErrorMessage: "",
		}
	}

	ticker := time.NewTicker(interval)

	for range ticker.C {
		fmt.Println("Starting a healthcheck round.")
		var wg sync.WaitGroup
		wg.Add(len(nodes))
		for name, url := range nodes {
			go func(name, url string) {
				defer wg.Done()
				statusCode, message, hcErr := healthCheck(name, url)
				lastState := ErrorStates.GetLastState(name)

				if hcErr != nil {
					if lastState.LastState == "error" && lastState.LastErrorMessage == hcErr.Error() {
						fmt.Printf("error when checking %s: %v", name, hcErr)
						return
					}
					_, ok := hcErr.(*OutOfSyncError)
					if ok {
						if err := notify(discordWebhook, PrettifyMsg(discordRole, name, "warning", "outOfSync", message)); err != nil {
							panic(err)
						}
						ErrorStates.UpdateState(name, "outOfSync", hcErr)
						return
					}
					var notifMsg string = fmt.Sprintf("error when checking %s: %v", name, hcErr)
					if err := notify(discordWebhook, PrettifyMsg(discordRole, name, "error", "error", notifMsg)); err != nil {
						panic(err)
					}
					fmt.Println(notifMsg)
					ErrorStates.UpdateState(name, "error", hcErr)
					return
				}
				if statusCode >= 200 && statusCode <= 204 {
					fmt.Printf("Node: %s, Status code: %v, %s\n", name, statusCode, message)
					if lastState.LastState == "error" || lastState.LastState == "outOfSync" {
						ErrorStates.UpdateState(name, "healthy", nil)
						if err := notify(discordWebhook, PrettifyMsg(discordRole, name, "success", "healthy", "back to normal")); err != nil {
							panic(err)
						}
					}
					return
				}
				if err := notify(discordWebhook, fmt.Sprintf("%s, %s", discordRole, message)); err != nil {
					panic(err)
				}
			}(name, url)
		}
		wg.Wait()
	}
}
